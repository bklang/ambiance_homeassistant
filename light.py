"""Platform for light integration."""
import logging
from ambiance_client import AmbianceClient

import voluptuous as vol

import homeassistant.helpers.config_validation as cv
# Import the device class from the component that you want to support
from homeassistant.components.light import (
    ATTR_BRIGHTNESS,
    ATTR_HS_COLOR,
    PLATFORM_SCHEMA,
    SUPPORT_BRIGHTNESS,
    SUPPORT_COLOR,
    Light)
#from homeassistant.const import CONF_HOST, CONF_PASSWORD, CONF_USERNAME

_LOGGER = logging.getLogger(__name__)

# Validation of the user's configuration
PLATFORM_SCHEMA = PLATFORM_SCHEMA.extend({
    #vol.Required(CONF_HOST): cv.string,
    #vol.Optional(CONF_USERNAME, default='admin'): cv.string,
    #vol.Optional(CONF_PASSWORD): cv.string,
})


def setup_platform(hass, config, add_entities, discovery_info=None):
    """Set up the Awesome Light platform."""

    ambiance_client = AmbianceClient('http://10.3.20.29:4000')
    lights = ambiance_client.get_entities()['lights']
    entities_to_add = []
    for light_id in lights:
        entities_to_add.append(AmbianceLight(ambiance_client, light_id, lights[light_id]))
    add_entities(entities_to_add)

class AmbianceLight(Light):
    """Representation of a light in the Ambiance server."""

    def __init__(self, ambiance_client, light_id, params):
        # TODO: Get light metadata (name, description, capabilities)
        self._name = params["name"]
        self._id = light_id
        self._client = ambiance_client
        self._is_on = False
        self._brightness = 0
        self._hs_color = [0.0, 0.0]

    @property
    def name(self):
        """Return the display name of this light."""
        return self._name

    @property
    def is_on(self):
        """Return true if light is on."""
        # TODO: Get the state from the Ambiance server
        return self._is_on

    @property
    def brightness(self):
        """Return the brightness of this light between 0..255."""
        return self._brightness

    @property
    def hs_color(self):
        """Return the hue and saturation color value [float, float]."""
        return self._hs_color

    @property
    def supported_features(self):
        """Flag supported features."""
        return SUPPORT_BRIGHTNESS | SUPPORT_COLOR

    def turn_on(self, **kwargs):
        """Instruct the light to turn on."""
        params = {}
        if ATTR_HS_COLOR in kwargs:
            params["hs_color"] = kwargs[ATTR_HS_COLOR]

        if ATTR_BRIGHTNESS in kwargs:
            params["brightness"] = kwargs[ATTR_BRIGHTNESS]

        self._client.turn_on(self._id, params)
        self._is_on = True

    def turn_off(self, **kwargs):
        """Instruct the light to turn off."""
        self._client.turn_off(self._id, {})
        self._is_on = False

    def update(self):
        """Fetch new state data for this light.

        This is the only method that should fetch new data for Home Assistant.
        """
        entities = self._client.get_entities()
        light = entities["lights"][self._id]
        self._is_on = light["is_on"]
        self._brightness = light["brightness"]
        self._hs_color = light["hs_color"]
